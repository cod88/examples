import wpi from 'wiring-op'
import thePin from "./thePin"
import gpioConfig from "../config/gpio.js"  // это конфиг пинов.
import {gpio as logger } from "../log"
import fs from "fs"
import net from "net"
import ISapi from "../ISapi"
import deviceInfo from "../system/deviceInfo.js"
import configVars from "../config/vars.js"

var curLevel = 0;
var preLevel = 0;

class GpioDispatcher {

	constructor() {

		this.pins = {} // object witch containts the objects of ThePin 
		this.pinsTestMode = false;
		this.inputPins = [0,0,0,0,0,0,0,0]; // array consists of digits, where every digit means one pin nimber
											// there are digits are divised in three equal group
											// 1 - output pins numbers, 2 - pins where transmits the called number, 3 - signal pin
		this.alterTubeConnectPin = gpioConfig.alterTubeConnectPin;
		this.callWasHappen = [0,0,0,0];
		this.wpi = wpi;
		this.wpi.setup('wpi');
		this.unixServer = null;

		// когда получена информация об устройстве запрашиваем информацию о пинах, включаем пины и запускаем
		// сервер для связи с gpioListener
		this.checkDeviceInfoReady(
			() => {
				let outPins = [];
				let relay;
				for (let rel in deviceInfo.relays) {
					relay = gpioConfig.relays[deviceInfo.relays[rel].NUM];
					outPins.push(relay.output );
					if (deviceInfo.relays[rel].PANEL_STATUS) {
						this.inputPins[rel] = relay.inputDigital;
						this.inputPins[Number(rel)+4] = relay.inputAnalog;
					}

				}

				this.makePinsConfig(outPins);
				this.startUnixSocketServer();				
			},
		 );
	}

	/*
		Обеспечение синглтон режима
	*/
	static get instance() {
		if (!this._instance) {
			this._instance = new GpioDispatcher;
		}
		return this._instance;
	}

	/*
		Метод проверяет наличие информации в объекте diviceInfo
		и если информация присутствует запускает функцию переданную в параметре
		В случае неудачи мы снова запускаем эту же функцию через 1 секунду (наша задача - дождаться появления информации)
	*/
	checkDeviceInfoReady(whenGood) {
	    if (!deviceInfo.mac || !deviceInfo.relays) {
	    	// console.log("Not ready");
	    	 setTimeout( ()=> { this.checkDeviceInfoReady(whenGood) }, 1000);
	    	return false;
	    }
	    whenGood();
	}

/*
	Method takes array of digits where every digits is the pin number 
	whitch should be configured as output pin. Then methods creates objects of thePin class
	and set parameters
*/
	makePinsConfig(pinAr) {
		for (let p=0; p<pinAr.length; p++) {
			if (pinAr[p] == 0) 
				continue;

			if (typeof this.pins[pinAr[p]] === "undefined" ) {
					this.pins[pinAr[p]] = new thePin(pinAr[p]);
				}
	
			this.setPinMode(pinAr[p], wpi.OUTPUT);
			this.setPinPullUpDnControl(pinAr[p], wpi.PUD_DOWN);
		}

		this.wpi.pinMode(this.alterTubeConnectPin, wpi.OUTPUT);
		this.wpi.digitalWrite(this.alterTubeConnectPin, this.wpi.LOW);
		// logger.log(this.pins);
	}

	setPinMode(id, mode) {
		if (mode == this.wpi.OUTPUT) 	// ничего не делаем с пином если это не output пин
										//	input пины мы слушаем отдельной утилитой
			this.wpi.pinMode(id, mode);
		this.pins[id].mode = mode;
	}

	setPinPullUpDnControl(id, value) {
		if (this.pins[id].mode == this.wpi.OUTPUT) // см коммент в методе setPinMode
			this.wpi.pullUpDnControl(id, value);
		this.pins[id].pullUpDnControl = value;
	}

	setPinUp(pid) {
		this.pins[pid].state = wpi.HIGH;
		this.wpi.digitalWrite(pid, this.wpi.HIGH)
	}

	setPinDown(pid) {
		this.pins[pid].state = wpi.LOW;
		this.wpi.digitalWrite(pid, this.wpi.LOW)
	}

	passThroughDoor(pid) {
		if (typeof this.pins[pid] === "undefined" ) {
			return "relay isn't connected";
		}
		if (this.pins[pid].busy) { logger.log("Pin ", pid ," is busy"); return "relay is busy"; }
		this.pins[pid].busy = true;
		this.setPinUp(pid);
		setTimeout(
			() => {
					this.setPinDown(pid);
					this.pins[pid].busy = false;
				},
			configVars.openDoorDuration
			);
		return true;
	}

	testPins() {
		if (!this.pinsTestMode) {
				this.pinsTestMode = true;
				for (let rel in gpioConfig.relays) {
					this.wpi.pinMode(gpioConfig.relays[rel].output, wpi.OUTPUT);
					this.wpi.pullUpDnControl(gpioConfig.relays[rel].output, wpi.PUD_DOWN);
				}
			}

		for (let rel in gpioConfig.relays) {
			this.wpi.digitalWrite(gpioConfig.relays[rel].output, this.wpi.HIGH)
			setTimeout(
					()=> {this.wpi.digitalWrite(gpioConfig.relays[rel].output, this.wpi.LOW)},
					1000
				);

		}			
	}

	startUnixSocketServer() {
		// Setting and start socket for interconnection with gpio listener
		if (fs.existsSync("/run/teledomofon-pipe.sock")) {
			logger.log("Delete sock file")
			fs.unlinkSync("/run/teledomofon-pipe.sock")
		}
		
		this.unixServer = net.createServer(
			(socket) => {
				socket.setTimeout(1000, ()=>{ console.log("closed by timeout"); } );
				socket.on('data', this.onDataFromGpioListener.bind(this, socket )	);
			}
		);

		this.unixServer.listen("/run/teledomofon-pipe.sock", 
			() => {
				logger.log("Unix socket listening started on "+this.unixServer.address());
				}
			);
	}

/*
	Этод метод вызывается когда получаются данные от программы слушающей gpio через сокет
	Определяет тип сообщения от клиента и отвечает
*/

	onDataFromGpioListener(socket, data) {
		let len = data.length;
		
		// если пришёл один байт, в котором число 112 (символ "p").
		// Это запрос клиента для конфигаурации пинов
		if ( (len == 1) && (data[0] == 112) ){
			logger.log("Requested pin info from gpio listener");
			socket.write(Buffer.from(this.inputPins));
			return 0;
		}

		if ( (len == 1) && (data[0] == 116 ) ) {
			logger.log("Requested number of pin for switch alternative tube on/off");
			socket.write(Buffer.from([gpioConfig.alterTubeConnectPin]));
			return 0;
		}
		// если пришло 5 байт, то это пришёл номер квартиры
		// первый и последний байты  - просто нули, 2 байт - номер пина, 3,4 байты - номер квартиры (одно число в 2 байтах)
		if (len == 5) {
			socket.write(Buffer.alloc(1, 1));
			let secByte = (data[3]) << 8; 	// это делается потому, что в 
											// третьем байте у нас передаётся второй 
											// байт числа номера квартиры

			
			/*
				Bellow string needed while wire to analog line from call panel
				is not connected. There is algorithm which don't interepts calling number 1
				if in last 60 sec any number was called
			*/
			if ( (data[2]+secByte === 1)  && (this.callWasHappen[data[1]+1] === 1)  ) {
				logger.log("Skip calling num 1");
				return 0;
			}
			this.callWasHappen[data[1]+1] = 1;
			setTimeout( ()=> {this.callWasHappen[data[1]+1] = 0; }, 60000 );

			// если случился вызов номера 112
			if ( (data[2]+secByte === 112 ) && (configVars.extraServiceOn) ){
				this.setPinUp(this.alterTubeConnectPin);
			}

			logger.log("Called: "+(data[2]+secByte ) + " on PIN num "+data[1] );
			ISapi.sendCalledNumber(data[1], data[2]+secByte )
				.then(	(res) => { },
						(rej) => {logger.log("Err: ", rej)}
					);
		}
		else {
			socket.write(Buffer.alloc(1, 0));
			logger.log("ERROR: Wrong number of bytes of data. I do nothing");
		}
	}

	refreshGpioSettings(settings) {
	    if (settings === false) return;
	    if (settings.getBody().relays)
	    	deviceInfo.relays = settings.getBody().relays;
	}


}

export default GpioDispatcher.instance