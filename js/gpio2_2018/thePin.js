
class Pin {

	constructor(id) {
		this.id = id
		this.mode = null
		this.pullUpDnControl = null 
		this.busy = false
		this.state = null
	}
}

export default Pin
