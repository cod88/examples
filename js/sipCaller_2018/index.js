import wpi from 'wiring-op'
import {sipCaller as logger} from '../log'
import gpioConfig from "../config/gpio.js"

class SipCaller {

    constructor() {
        this.intWatchForCall = null
        this.callWatchPin   = gpioConfig.callWatchPin       // pin connected with groud through optron witch closing when calling (7)
        this.callAnswerPin  = gpioConfig.callAnswerPin      // pin witch is connected to optron witch is closing way with 50 Ohm (like a gercon) (9)
                                                            // in non call mode this pin has 1 value, and when tube should be up, pin set to 0 value
        this.tubeConnectPin = gpioConfig.alterTubeConnectPin     // pin wich manages of tube connecting to line. it should be set in 0 when call is over
        this.DoorCallStatus = null

        this.wpi = wpi
        this.wpi.setup('wpi')

        this.sipAccount = null
        this.sipCall = null
        this.sipster = require('sipster')
        this.sipster.init()
        var sipTransport = new this.sipster.Transport({port: 5060})
        logger.log("Sipster make transport")
        // console.log(sipTransport.getInfo())


        this.wpi.pinMode(this.callAnswerPin, this.wpi.OUTPUT) // пин через который поднимаем трубку

        this.sipster.start()
        // logger.log(this.sipster.getDevices())

        this.startWatchForCall()
        this.setTubeState('down')

        this.registerAccount.bind(this, this.sipster)()


    }

    /*
        Set pin in input mode
        run interval loop with function which check pin state and decide what should to do
    */
    startWatchForCall() {
        this.wpi.pinMode(this.callWatchPin, this.wpi.INPUT)
        this.wpi.pullUpDnControl(this.callWatchPin, wpi.PUD_UP); //it means you should connect this pin with 0V for get 0 value, else it have 1 value
        this.intWatchForCall = setInterval(this.callWatchPinAnalizer.bind(this),1000)
    }

    /*
        
    */
    callWatchPinAnalizer() {
        let callSt = this.wpi.digitalRead(this.callWatchPin)

        if (this.DoorCallStatus != callSt ) {
            this.DoorCallStatus = callSt
            if (callSt == 0) {
                logger.log("The inbound call from door")
                setTimeout( (() => { this.setTubeState('up') }).bind(this), 1000)
                this.makeCall.call(this)
            }
            if (callSt == 1) {
                logger.log("Call from door finished")
                this.setTubeState('down')
                this.closeCall.call(this)
            }
        }
    }

    setTubeState(state = 'down') {

        switch (state) {
            case 'down':
                logger.log("switch tube in down")
                this.wpi.digitalWrite(this.callAnswerPin, this.wpi.HIGH)
                setTimeout( (() => { this.wpi.digitalWrite(this.tubeConnectPin, this.wpi.LOW) } ).bind(this), 1000 )
                this
            break

            case 'up':
                logger.log("switch tube in up")
                this.wpi.digitalWrite(this.callAnswerPin, this.wpi.LOW)
            break
        }

    }

    registerAccount(sipster) {
        let sipAccount = new sipster.Account({
            idUri: 'sip:[username]@[server_ip]:[port]',
            regConfig: {
            registrarUri: 'sip:[server_ip]:[port]',
            timeoutSec: 300
              },
            sipConfig: {
                authCreds: [{
                  scheme: 'Digest',
                  realm: 'asterisk',
                  username: 'username',
                  dataType: 0,
                  data: 'password'
                }]
              }
        })

        sipAccount.sipster = sipster

        sipAccount.on('registering', function () {
            logger.log('Account registering');
        });
        sipAccount.on('registered', function () {
            logger.log('Account registered');
        });

        sipAccount.on('state', function (state) {
            logger.log('GET NEW state: ' + state + " --- ");
        });

        this.sipAccount = sipAccount
    }

    makeCall() {
        this.sipster.setCaptureDev(12);
        this.sipster.setPlaybackDev(12);
        this.sipster.setInputVolume(100);
        this.sipster.setOutputVolume(100);

        this.sipCall = this.sipAccount.makeCall("sip:[username]@[server_ip]:[port]")

        this.sipCall.on('media', (medias) => {
            logger.log("=== MEDIA CAME ===")
            medias[0].startTransmitTo(this.sipster.getPlaybackDevMedia());
            this.sipster.getCaptureDevMedia().startTransmitTo(medias[0]);
        })

        this.sipCall.on('state', (state) => {
            logger.log("Call state changed to "+ state)
            if (state === 'disconnected') {
                logger.log("Call over")
                this.setTubeState('down')
            }
        })

    }

    closeCall() {
        logger.log("Closing the call")
        if (this.sipCall !== null && this.sipCall.isActive) {
            this.sipCall.hangup()
        }
    }

}

export default SipCaller