<?php

namespace app\modules\weatherParser\logtargets;

use yii\log\Target;

class ConsoleTarget extends Target
{
	
	public function export()
	{
		foreach ($this->messages as $line) {
			echo $line[0]."\n";
		}
	}
}