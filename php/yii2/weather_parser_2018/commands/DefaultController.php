<?php

namespace app\modules\weatherParser\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\httpclient\Client;

use yii\db\Query;

class DefaultController extends Controller
{

	const ERROR = 80;
	const WARNING = 60;
	const INFO = 40;
	const TRACE = 20;
	
	const LOG_LEVELS = [
		'error' 	=> self::ERROR,
		'warning' 	=> self::WARNING,
		'info'		=> self::INFO,
		'trace'		=> self::TRACE,
	];

	const DAY = 1;
	const NIGHT = 0;

	public $cityId;
	public $cityName;

	public $verbose;


	public function init()
	{
		parent::init();
		$this->cityId = $this->module->cityId;
		$this->cityName = $this->module->cityName;

		$this->on(\yii\console\Controller::EVENT_BEFORE_ACTION, function(){
			$this->verbose = isset(self::LOG_LEVELS[$this->verbose]) ? self::LOG_LEVELS[$this->verbose] : self::LOG_LEVELS['info'];
			
			$levels = [];
			foreach (self::LOG_LEVELS as $name => $value ) {
				if ($value >= $this->verbose) {
					$levels[] = $name;
				}
			}

			\Yii::$app->log->targets['parser'] = \Yii::createObject([
				'class' => 'yii\log\FileTarget',
				'class' => 'app\modules\weatherParser\logtargets\ConsoleTarget',
				'levels' => $levels,
				'categories' => ['app\modules\*'],
				'logVars' => [],
			]);
			
		});
	}

	public function options($actionID)
	{
		return [
			'cityId', 'cityName', 'verbose'
		];
	}

	public function actionIndex()
	{
		echo "\n\nThere is module for parse site http://www.meteorf.ru/
and store gotten data to DB
Params:
	for all methods:
	[--verbose=<trace|info|warning|error>]

Methods:
	default/parse [--cityId=<City ID from meteorf.ru (used for request to site)> --cityName=<City name (used just for fill up database field)>]\n\n";
		return ExitCode::OK;
	}

	private function parsePage($html)
	{
		$doc = \phpQuery::newDocumentHTML($html);
		$trs = $doc->find('table.weather-data tbody tr');

		$fcArray = [];
		foreach ($trs as $tr) {
			$tr = pq($tr);
			$date = substr($tr->attr('id'), 13, -6);
			$tds = $tr->find('td')->elements;

			if (count($tds) == 8) array_shift($tds);

			$subArrayOfDate = [];
			$tempTd = pq( $tds[0] );
			$timeOfDay = $tempTd->find('div.small')->html();

			//температура
			$subArrayOfDate['temp'] = trim($tempTd->find('div.big')->html(), "\n " );
			// Облачность и осадки
			$subArrayOfDate['cloud_cover'] = trim($tds[2]->nodeValue, "\n " );
			// Давление
			$subArrayOfDate['pressure'] = trim($tds[3]->nodeValue, "\n " );
			// Количество осадков
			$subArrayOfDate['precipitation'] = trim($tds[4]->nodeValue, "\n " );
			// Вероятность осаков
			$subArrayOfDate['precipitation_chance'] = trim($tds[5]->nodeValue, "\n " );
			// Ветер
			$subArrayOfDate['wind'] = trim($tds[6]->nodeValue, "\n " );
			
			// вносим подмассив в основной массив
			$subArrayKey = (strpos($timeOfDay, 'днем') === false ) ? self::NIGHT : self::DAY;
			$fcArray[$date][$subArrayKey] = $subArrayOfDate;
			
		}
		
		return $fcArray;	
	}

	private function storeWeatherToDb($forecast)
	{
		\Yii::trace("\tполучение имеющихся записей", __METHOD__);
		$forecastsInDB = (new Query())
			->select(['id', 'city', 'city_id', 'date', 'time_of_day'])
			->from('ecor_monitor.weather_forecast')
			->where(['>=', 'date', date('Y-m-d')])
			->andWhere(['city_id'=>$this->module->cityId])
			->indexBy(function($row) { return $row['date']."-".$row['time_of_day']; })
			->all();
		\Yii::trace("\tOK", __METHOD__);

		\Yii::trace("\tОбработка и сохранение...", __METHOD__);
		foreach ($forecast as $date => $items) {
			$date = substr_replace($date, "-", 4, 0);
			$date = substr_replace($date, "-", 7, 0);
		
			foreach ($items as $time_of_day => $item) {
				$index = $date."-".$time_of_day; 	// индекс элемента в массиве полученного из БД
													// хранящего прогноз на дату и время суток

				preg_match('/([СЮЗВ]+)\D+(\d+)/', $item['wind'], $wind_parts);
			
				if (isset($forecastsInDB[$index])) { // если такой элемент уже есть, то обновляем
					$command = \Yii::$app->db->createCommand()->update($this->module->forecastTablename,[
                    	'temp' => $item['temp'],
                    	'cloud_cover' => $item['cloud_cover'],
                    	'pressure' => $item['pressure'],
                    	'precipitation' => $item['precipitation'],
                    	'precipitation_chance' => $item['precipitation_chance'],
                    	'wind_direction' => $wind_parts[1],
                    	'wind_speed' => $wind_parts[2],
	                ], 
	                ['id' => $forecastsInDB[$index]['id']]);
				}
				else { // если нет, то создаём
					$command = \Yii::$app->db->createCommand()->insert($this->module->forecastTablename,[
	                	'city' => $this->module->cityName,
	                	'city_id' => $this->module->cityId,
	                	'date' => $date,
	                	'time_of_day' => $time_of_day,
	                	'temp' => $item['temp'],
	                	'cloud_cover' => $item['cloud_cover'],
	                	'pressure' => $item['pressure'],
	                	'precipitation' => $item['precipitation'],
	                	'precipitation_chance' => $item['precipitation_chance'],
	                	'wind_direction' => $wind_parts[1],
	                	'wind_speed' => $wind_parts[2],
                	]);

				}
            	// \Yii::trace( "\t\tExec: ".$command->getRawSql(), __METHOD__ );
            	try {
            		$command->execute();
            		// \Yii::trace("OK", __METHOD__);
            	} catch (\Exception $e) {
            		\Yii::error("Ошибка сохранения в БД: ".$e->getMessage(), __METHOD__);
            		return false;
            	}
			}
		}

		return true;
	}

	public function actionParse()
	{
		$client = new Client(['baseUrl' => $this->module->url]);
		$request = $client->createRequest()
			->setMethod('GET');
		
		\Yii::trace("Делаем запрос, чтобы получить куку местоположения...", __METHOD__);
		// получаем значение куки необходимой для задания города		
		$resp = $request->send();
		try {
            $resp = $request->send();
            if (($httpCode = $resp->getHeaders()->get('http-code')) != 200) {
                throw new \Exception("Http code:".$httpCode, 1);
            }
            \Yii::trace("OK", __METHOD__);
        } catch (\Exception $e) {
        	\Yii::error("Ошибка HTTP запроса: ".$e->getMessage, __METHOD__);
            return ExitCode::UNSPECIFIED_ERROR;
        }

		$cookIp = $resp->getCookies()->get('BITRIX_SM_location_user_ip')->value;

		$request->setCookies([
				['name' => 'BITRIX_SM_location_user_city', 'value'=>$this->module->cityId],
				['name' => 'BITRIX_SM_location_search_true', 'value'=>1],
				['name'	=> 'BITRIX_SM_location_user_ip', 'value'=>$cookIp ],
		]);
		
		\Yii::trace("Запрос данных...", __METHOD__);
		try {
			$html = $request->send()->content;
			\Yii::trace("OK", __METHOD__);
		} catch (\Exception $e) {
			\Yii::error("Ошибка HTTP запроса: ".$e->getMessage, __METHOD__);
			return ExitCode::UNSPECIFIED_ERROR;
		}

		\Yii::trace("парсинг данных...", __METHOD__);
		$forecast = $this->parsePage($html);
		\Yii::trace("ОК", __METHOD__);
		
		\Yii::trace("Сохранение в БД...", __METHOD__);
		if ($this->storeWeatherToDb($forecast) === true ) {
			\Yii::trace("OK", __METHOD__);
		} else {
			return ExitCode::UNSPECIFIED_ERROR;
		}
		return ExitCode::OK;
	}
}