from typing import Any

from pydantic import BaseModel, validator

from app.classes.enum import EnumObject

RESPONSE = EnumObject()
RESPONSE.OK = "ok"
RESPONSE.ERROR = "error"

SESSION_MESSAGE_TYPES = EnumObject()
SESSION_MESSAGE_TYPES.RESPONSE = "response"
SESSION_MESSAGE_TYPES.UPDATE = "update"


class MessageSessionToClient(BaseModel):
    type: str = SESSION_MESSAGE_TYPES.RESPONSE
    request_id: str
    status: str
    data: Any

    @validator("type")
    def validate_type(cls, type):
        if type != SESSION_MESSAGE_TYPES.RESPONSE:
            raise ValueError("Type value error, type value must be 'response'")
        return type

    @validator("status")
    def validate_status(cls, status):
        if status not in RESPONSE.get_values():
            raise ValueError("Status value error, status value must be 'ok' or 'error'")
        return status


UPDATE_CLIENT_TYPES = EnumObject()
UPDATE_CLIENT_TYPES.GAME_INFO = 'game_info'


class UpdateDataObject(BaseModel):
    type: str
    data: Any

    @validator('type')
    def validate_type(cls, type):
        if type not in UPDATE_CLIENT_TYPES.get_values():
            raise ValueError(f'UpdateDataObject type value error, status value '
                             f'must be in {UPDATE_CLIENT_TYPES.get_values()}')
        return type


class UpdateMessageSessionToClient(BaseModel):
    type: str = SESSION_MESSAGE_TYPES.UPDATE
    data: UpdateDataObject

    @validator("type")
    def validate_type(cls, type):
        if type != SESSION_MESSAGE_TYPES.UPDATE:
            raise ValueError("Type value error, type value must be 'update'")
        return type


class MessageClientToSession(BaseModel):
    action: str
    request_id: str
    data: Any


class HttpRequestToMessage(BaseModel):
    username: str
    message: UpdateMessageSessionToClient
