from app.classes.enum import EnumObject

from app.classes.connection_handler.classes.actions.main import echo
from app.classes.connection_handler.classes.actions.users import auth_user
from app.classes.connection_handler.classes.actions.games import create_game, get_game, game_list, game_action

CONNECTION_HANDLER_ACTIONS = EnumObject()
CONNECTION_HANDLER_ACTIONS.ECHO = 'echo'
CONNECTION_HANDLER_ACTIONS.AUTH_USER = 'auth_user'
CONNECTION_HANDLER_ACTIONS.CREATE_GAME = 'create_game'
CONNECTION_HANDLER_ACTIONS.GET_GAME = 'get_game'
CONNECTION_HANDLER_ACTIONS.GAME_LIST = 'game_list'
CONNECTION_HANDLER_ACTIONS.GAME_ACTION = 'game_action'

actions_map = {
        CONNECTION_HANDLER_ACTIONS.ECHO: echo,
        CONNECTION_HANDLER_ACTIONS.AUTH_USER: auth_user,
        CONNECTION_HANDLER_ACTIONS.CREATE_GAME: create_game,
        CONNECTION_HANDLER_ACTIONS.GET_GAME: get_game,
        CONNECTION_HANDLER_ACTIONS.GAME_LIST: game_list,
        CONNECTION_HANDLER_ACTIONS.GAME_ACTION: game_action,
    }
