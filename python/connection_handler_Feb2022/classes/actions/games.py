import json
from typing import Dict, List
import aiohttp

from app.classes.types.GameInfo import GameInfo
from app.classes.core_requester.GameCoreRequester import GameCoreRequester
from app.classes.entity_requester.UniversalEntityRequester import UniversalEntityRequester
from app.classes.entity_requester.GameRequester import GameRequester
from app.classes.types.GameInfo import GameActionParams


async def create_game(session, data: Dict) -> GameInfo:
    core_req = GameCoreRequester()
    game_info = await core_req.async_create_game(data=data)
    return GameInfo(**game_info)


async def get_game(session, data: Dict) -> GameInfo:
    ent_req = UniversalEntityRequester()
    game_info = ent_req.find(entity_type="game", pk_value=data.get("game_id"))
    return GameInfo(**game_info)


async def game_list(session, data: Dict) -> List[GameInfo]:
    ent_req = GameRequester()
    games_info = ent_req.get_games_list(params=data)
    return [GameInfo(**i) for i in games_info]


async def game_action(session, data: Dict) -> GameInfo:
    game_action_params = GameActionParams(**data)
    core_req = GameCoreRequester()

    game_info = await core_req.async_game_action(
        game_id=game_action_params.game_id,
        action_name=game_action_params.action_name,
        data=game_action_params.data
    )
    return GameInfo(**game_info)
