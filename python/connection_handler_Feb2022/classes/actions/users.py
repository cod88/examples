from typing import Dict

from app.classes.config import get_config
from app.classes.core_requester.UserCoreRequester import UserCoreRequester
from app.classes.types.UserInfo import UserInfo


async def auth_user(session, data: Dict) -> UserInfo:
    config = get_config("CONNECTION_HANDLER")
    data['connection_handler_name'] = config.get("service_name")
    core_req = UserCoreRequester()
    user: UserInfo = await core_req.async_auth_user(data=data)
    session.user = user.login
    return user

