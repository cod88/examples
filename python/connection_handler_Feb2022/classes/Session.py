import asyncio
from asyncio import Queue
from typing import Any, Callable, Union

from app.classes.connection_handler.classes.actions.map import actions_map
from app.classes.connection_handler.exceptions import ActionException, UnknownAction
from app.classes.connection_handler.classes.TransportConnection import TransportConnection
from app.classes.connection_handler.classes.types.SessionMessages import MessageSessionToClient, \
    MessageClientToSession, RESPONSE, SESSION_MESSAGE_TYPES, UpdateMessageSessionToClient


class Session:

    _transport: TransportConnection = None
    _output_queue: Queue = None
    is_started: bool = None
    user: str = None

    def __init__(self, transport_connection: TransportConnection):
        self._transport = transport_connection
        self._output_queue = Queue()
        self.is_started = False

    async def _input(self):
        while True:
            try:
                message: MessageClientToSession = await self._transport.recv_msg()
                action_func: Callable[[Session, Any], Any] = actions_map.get(message.action)
                if action_func is None:
                    raise UnknownAction(f"Action {message.action} is not exist")

                try:
                    data_out = await action_func(self, message.data)
                except Exception as e:
                    raise ActionException(f"Action exception, error {e}")

            except (UnknownAction, ActionException) as e:
                await self._transport.send_msg(
                    MessageSessionToClient(
                        type=SESSION_MESSAGE_TYPES.RESPONSE,
                        request_id=message.request_id,
                        status=RESPONSE.ERROR,
                        data=str(e)
                    ))
                continue
            except Exception as e:
                await self._transport.send_msg(
                    MessageSessionToClient(
                        type=SESSION_MESSAGE_TYPES.RESPONSE,
                        request_id=message.request_id,
                        status=RESPONSE.ERROR,
                        data=str(e)
                    ))
                break
            else:
                await self._transport.send_msg(
                    MessageSessionToClient(
                        type=SESSION_MESSAGE_TYPES.RESPONSE,
                        request_id=message.request_id,
                        status=RESPONSE.OK,
                        data=data_out
                    ))

    async def _output(self):
        data = await self._output_queue.get()
        while data:
            await self._transport.send_msg(data)
            data = await self._output_queue.get()

    async def send_message(self, data: UpdateMessageSessionToClient):
        await self._output_queue.put(data)

    async def start(self):
        self.is_started = True
        await asyncio.gather(self._input(), self._output())
