from typing import Dict
from abc import abstractmethod
from hashlib import md5
from random import randint
from typing import Union

from app.classes.connection_handler.classes.IListenerObserver import IListenerObserver
from app.classes.connection_handler.classes.TransportConnection import TransportConnection


class TransportListener:

    observer: Union[IListenerObserver, None] = None
    connections: Dict[str, TransportConnection] = {}

    def __init__(self, observer: Union[IListenerObserver, None] = None):
        self.observer = observer

    @abstractmethod
    async def listen(self):
        pass

    @abstractmethod
    def configure(self, config=None):
        pass

    def gen_connection_id(self) -> str:
        connection_id: str = md5(str(randint(0, 10000)).encode()).hexdigest()
        if connection_id in self.connections:
            connection_id = self.gen_connection_id()

        return connection_id

    def connection_connected(self, connection_id: str):
        connection: TransportConnection = self.connections[connection_id]
        self.observer.new_connection(connection)

    def connection_disconnected(self, connection_id: str):
        # TODO сделать корректную обработку
        del self.connections[connection_id]
