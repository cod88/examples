from abc import abstractmethod

from app.classes.connection_handler.classes.TransportConnection import TransportConnection


class IListenerObserver:

    @abstractmethod
    def new_connection(self, transport_connection: TransportConnection):
        pass

    @abstractmethod
    def connection_lost(self, transport_connection: TransportConnection):
        pass
