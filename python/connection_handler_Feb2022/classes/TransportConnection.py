from asyncio import Queue
from typing import Any


class TransportConnection:

    input_queue: Queue = None
    output_queue: Queue = None

    def __init__(self, input_queue: Queue = None, output_queue: Queue = None):
        self.input_queue = input_queue
        self.output_queue = output_queue

    async def send_msg(self, data: Any):
        await self.output_queue.put(data)

    async def recv_msg(self):
        return await self.input_queue.get()

    def is_complete(self):
        return bool(self.input_queue and self.output_queue)
