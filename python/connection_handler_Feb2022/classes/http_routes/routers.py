from fastapi import APIRouter, Response, Request

from app.classes.config import get_config
from app.classes.connection_handler.decorators import exception_handler, return_data_formatter
from app.classes.connection_handler.schemas import ResponseWrapper, ConnectionHandlerInfo
from app.classes.connection_handler.classes.types.SessionMessages import HttpRequestToMessage, UpdateMessageSessionToClient


router = APIRouter()


@router.get("/", response_model=ResponseWrapper)
@exception_handler
@return_data_formatter()
async def get_service_name(response: Response) -> ResponseWrapper:
    config = get_config("CONNECTION_HANDLER")
    data = ConnectionHandlerInfo(**{"name": config.get("service_name")})
    return data


@router.post("/send_message", response_model=ResponseWrapper)
@exception_handler
@return_data_formatter()
async def send_message_to_user(response: Response, request: Request, message_data: HttpRequestToMessage):
    session_list = [s for s in request.app.connection_handler_instance.sessions if s.user == message_data.username]
    if len(session_list) == 0:
        raise ValueError(f'Session for user {message_data.username} is not found')

    session = session_list[0]

    message: UpdateMessageSessionToClient = message_data.message
    await session.send_message(data=message)
    return 'ok'
