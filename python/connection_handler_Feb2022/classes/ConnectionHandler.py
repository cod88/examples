import asyncio
from typing import List

from fastapi import FastAPI
import uvicorn

from app.classes.config import get_config
from app.classes.connection_handler.classes.http_routes.routers import router
from app.classes.connection_handler.classes.IListenerObserver import IListenerObserver
from app.classes.connection_handler.classes.TransportConnection import TransportConnection
from app.classes.connection_handler.classes.Session import Session
from app.classes.connection_handler.classes.TransportListener import TransportListener


class ChFastAPI(FastAPI):

    connection_handler_instance = None


class ConnectionHandler(IListenerObserver):
    """
        Performs handling of clients connections
    """

    sessions: List[Session]

    def __init__(self, transport: TransportListener):
        self.sessions = []
        self.transport = transport

    async def start_process(self):
        """
        Check if instance is ready to start and starts
        transport listener and http server of connection handler.
        Run asynchronously self._transport.listen and self._start_http.


        * use asyncio.gather
        :return:
        """

        await asyncio.gather(self.transport.listen(), self._start_http())

    def new_connection(self, transport_connection: TransportConnection):
        sess: Session = Session(transport_connection)
        self.sessions.append(sess)
        asyncio.create_task(sess.start())

    def connection_lost(self, transport_connection: TransportConnection):
        # TODO реализовать удаление сессии
        pass

    async def _start_http(self):
        config = get_config("CONNECTION_HANDLER")
        app = ChFastAPI()
        app.connection_handler_instance = self
        app.include_router(router)
        uv_conf = uvicorn.Config(app=app, host="localhost", port=int(config.get("http_server_port")))
        uv_server = uvicorn.Server(config=uv_conf)
        await uv_server.serve()
