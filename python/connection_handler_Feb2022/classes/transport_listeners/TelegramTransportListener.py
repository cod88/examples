from asyncio import Queue
import asyncio
from typing import Dict, Optional, Any

from aiogram import Bot, Dispatcher, types

from app.classes.config import get_config
from app.classes.connection_handler.classes.TransportListener import TransportListener
from app.classes.connection_handler.classes.TransportConnection import TransportConnection
from app.classes.connection_handler.exceptions import WrongConfig
from app.classes.connection_handler.classes.types.SessionMessages import MessageClientToSession


class TelegramTransportListener(TransportListener):

    dp: Dispatcher = None

    api_key: str = None
    """Api key for telegram bot"""

    def configure(self, config: Optional[Dict[str, Any]] = None) -> None:
        if config is None:
            config = get_config("CONNECTION_HANDLER")
        self.api_key = config.get("telegram_api_key")
        if not self.api_key:
            raise WrongConfig("Telegram api key doesnt exist")

        self.dp = Dispatcher(Bot(token=self.api_key))

    async def listen(self) -> None:
        async def send_output(message: types.Message) -> None:
            while True:
                q_data = await self.connections[message.chat.id].output_queue.get()
                await self.dp.bot.send_message(
                    chat_id=message.chat.id,
                    text=q_data.data
                )

        async def send_input(message: types.Message) -> None:
            data: MessageClientToSession = MessageClientToSession(
                action="echo",
                request_id=str(message.message_id),
                data=message.text
            )
            await self.connections[message.chat.id].input_queue.put(data)

        async def start_telegram_bot() -> None:
            disp: Dispatcher = self.dp

            @disp.message_handler()
            async def echo_message(message: types.Message) -> None:
                if message.chat.id not in self.connections:
                    self.connections[message.chat.id]: TransportConnection = TransportConnection(
                        input_queue=Queue(), output_queue=Queue()
                    )
                    self.connection_connected(message.chat.id)
                    await asyncio.gather(send_input(message), send_output(message))
                else:
                    await send_input(message)

            await asyncio.gather(disp.start_polling())

        await asyncio.gather(start_telegram_bot())
