import asyncio
from typing import Any
from asyncio import Queue
import json
import pydantic
import socket

from app.classes.config import get_config
from app.classes.connection_handler.classes.TransportListener import TransportListener
from app.classes.connection_handler.classes.TransportConnection import TransportConnection
from app.classes.connection_handler.classes.types.SessionMessages import MessageClientToSession


class MessageData(pydantic.BaseModel):
    connection_id: str
    data: Any


class TestTcpTransportListener(TransportListener):
    """
        Test transport listener
    """
    test_tcp_listener_input_port: int = None
    test_tcp_listener_output_port: int = None

    def configure(self, config=None):
        if not config:
            config = get_config("CONNECTION_HANDLER")
        self.test_tcp_listener_input_port = config['test_transport_listener_tcp_input_port']
        self.test_tcp_listener_output_port = config['test_transport_listener_tcp_output_port']

    async def listen(self):

        async def input_socket_handler(reader: asyncio.streams.StreamReader, writer: asyncio.streams.StreamWriter):
            data = await reader.read(4096)
            data = json.loads(data)
            message: MessageData = MessageData(**data)

            connection_created: bool = False

            if message.connection_id not in self.connections:
                self.connections[message.connection_id] = TransportConnection(input_queue=Queue(), output_queue=Queue())
                self.connection_connected(message.connection_id)
                connection_created = True

            data_to_session: MessageClientToSession = MessageClientToSession(
                action=message.data['action'],
                request_id=message.data['request_id'],
                data=message.data['data']
            )
            await self.connections[message.connection_id].input_queue.put(data_to_session)
            writer.write(json.dumps({'connection_created': connection_created, 'result': 'ok'}).encode())
            await writer.drain()
            writer.close()

            if connection_created:
                while True:
                    data = await self.connections[message.connection_id].output_queue.get()
                    message: MessageData = MessageData(connection_id=message.connection_id, data=data)
                    sock = socket.create_connection(('127.0.0.1', self.test_tcp_listener_output_port))
                    sock.send(message.json().encode())
                    sock.close()
        input_server = await asyncio.start_server(input_socket_handler, '127.0.0.1', self.test_tcp_listener_input_port)

        async def run_socket_server():
            async with input_server:
                await input_server.serve_forever()

        await asyncio.gather(run_socket_server())
