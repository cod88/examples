import websockets
import asyncio
from typing import List
from asyncio import Queue
from websockets.server import WebSocketServerProtocol

from app.classes.config import get_config
from app.classes.connection_handler.classes.TransportListener import TransportListener
from app.classes.connection_handler.classes.TransportConnection import TransportConnection
from app.classes.connection_handler.classes.Session import MessageClientToSession


class DualWSTransportListener(TransportListener):

    listen_domain: str = None
    listen_port: int = None

    def configure(self, config=None):
        if config is None:
            config = get_config("CONNECTION_HANDLER")
        self.listen_domain = config['ws_ip']
        self.listen_port = config['ws_port']

    async def listen(self):

        async def close_ws(ws: WebSocketServerProtocol, reason: str):
            await ws.send(reason)
            await ws.close()

        async def handler(ws: WebSocketServerProtocol, path):
            command_str: str = await ws.recv()

            command_list: List[str] = command_str.split(":")

            if len(command_list) != 2:
                await close_ws(ws, 'Bad command')
                return

            connection_id: str = command_list[0]
            channel_type: str = command_list[1]

            if connection_id:
                if connection_id not in self.connections:
                    await close_ws(ws, 'Connection is is not found')
                    return
            else:
                connection_id: str = self.gen_connection_id()
                self.connections[connection_id]: TransportConnection = TransportConnection()

            channel_types_map = {
                "input": "input_queue",
                "output": "output_queue",
            }

            if getattr(self.connections[connection_id], channel_types_map.get(channel_type)):
                await close_ws(ws, 'This type connection already exists for this connection_id')
                return

            setattr(self.connections[connection_id], channel_types_map.get(channel_type), Queue())

            await ws.send(f"OK:{connection_id}")

            if self.connections[connection_id].is_complete():
                self.connection_connected(connection_id)

            if channel_type == "input":
                data = True
                while data:
                    try:
                        data = await ws.recv()
                    except websockets.exceptions.WebSocketException as e:
                        break
                    try:
                        await self.connections[connection_id].input_queue.put(MessageClientToSession.parse_raw(data))
                    except Exception as e:
                        break

            if channel_type == "output":
                data = True
                while data:
                    try:
                        data = await self.connections[connection_id].output_queue.get()
                    except Exception as e:
                        break
                    try:
                        await ws.send(data.json())
                    except websockets.exceptions.WebSocketException as e:
                        break

            self.connection_disconnected(connection_id=connection_id)

        async def server():
            async with websockets.serve(handler, self.listen_domain, self.listen_port):
                await asyncio.Future()

        await server()
