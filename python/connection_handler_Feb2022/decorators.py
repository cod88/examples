from functools import wraps

from app.classes.connection_handler.schemas import ResponseWrapper


def exception_handler(function):
    """
    Handle exceptions raised in a function

    If exception is raised this decorator handle it and return
    data in general application response format
    which can be returned to user

    :param function:
    :return: jsonified information in general response format of application
    """

    @wraps(function)
    async def wrapper(*args, **kwargs):
        try:
            return await function(*args, **kwargs)
        except Exception as e:
            response = kwargs.get('response')
            response.status_code = getattr(e, 'http_code', 500)
            return {'data': f"Exception raised: { repr(e) }"}

    wrapper.__name__ = f"exception_handled_{function.__name__}"
    return wrapper


def return_data_formatter(ok_status=200):
    """
    Cast result of function to general response format

    :return: jsonified object and http status code
    """
    def _return_data_formatter(function):
        @wraps(function)
        async def wrapper(*args, **kwargs):
            resp = kwargs.get('response')
            resp.status_code = ok_status
            data = await function(*args, **kwargs)
            return ResponseWrapper(**{"data": data})

        wrapper.__name__ = f"return_data_formatter_{function.__name__}"
        return wrapper

    return _return_data_formatter
