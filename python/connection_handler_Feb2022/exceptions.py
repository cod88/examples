class ConnectionHandlerException(Exception):
    http_code = 500


class WrongConfig(ConnectionHandlerException):
    pass


class UnknownAction(ConnectionHandlerException):
    pass


class ActionException(ConnectionHandlerException):
    pass
