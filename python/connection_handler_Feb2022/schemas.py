from typing import Union

from pydantic import BaseModel


class ConnectionHandlerInfo(BaseModel):
    name: str


class ResponseWrapper(BaseModel):
    data: Union[ConnectionHandlerInfo, str]
