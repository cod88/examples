import asyncio

from app.classes.connection_handler.classes.ConnectionHandler import ConnectionHandler
from app.classes.connection_handler.classes.transport_listeners.DualWSTransportListener import DualWSTransportListener
from app.classes.connection_handler.classes.transport_listeners.TelegramTransportListener import TelegramTransportListener
from app.classes.connection_handler.classes.transport_listeners.TestTcpTransportListener import TestTcpTransportListener
from app.classes.connection_handler.exceptions import WrongConfig
from app.classes.config import get_config


transport_type_class_map = {
    "websocket": DualWSTransportListener,
    "telegram": TelegramTransportListener,
    "test_tcp": TestTcpTransportListener
}


def build_connection_handler(config: dict = None) -> ConnectionHandler:
    """
    1. Get config. If `config` arguments is None get config from application config
    2. Create transport listener basing on config. Choose concrete TransportListener realization, and
    create an instance.
    3. Create ConnectionHandler and set its transport listener instance
    4. Set ConnectionHandler instance as observer for TransportListener instance
    5. Return created ConnectionHandler
    :param config:
    :return:
    """
    if config is None:
        config = get_config("CONNECTION_HANDLER")
    transport_class = transport_type_class_map[config['transport']]
    if transport_class is None:
        raise WrongConfig(f"No such transport class")
    transport_listener = transport_class()
    connection_handler_instance = ConnectionHandler(transport_listener)
    transport_listener.observer = connection_handler_instance
    transport_listener.configure(config)

    return connection_handler_instance


async def start_connection_handler():
    """
    Use build_connection_handler for get ConnectionHandler instance
    Run connection handler's start_process
    :return:
    """
    connection_handler_instance: ConnectionHandler = build_connection_handler()
    await asyncio.gather(connection_handler_instance.start_process())


def main():
    asyncio.run(start_connection_handler())


if __name__ == "__main__":
    main()
