"""
Package implements service Connection Handler

UML diagram:

    ::

                        +―――――――――――――――――――――――――――――――――+
                        |   ConnectionHandler             |― ― ― ― ― ― ― ―+
                        +=================================+               |
                        | + sessions: List[Session]       |◇―――+
         +―――――――――――――◆| + transport: TransportListener  |    |          |
         |              +―――――――――――――――――――――――――――――――――+    |
         |              | + start_process()               |    |          |
         |              | + new_connection(               |    |
         |              |   transport_connection:         |    |          |
         |              |   TransportConnection)          |    |
         |              | + connection_lost()             |    |          |
         |              |   transport_connection:         |    |
         |              |   TransportConnection)          |    |          |
         |              +―――――――――――――――――――――――――――――――――+    |
         |                                                     |          |
         |                                                     |
         |                                                     |          |
         |              +―――――――――――――――――――――――――――――――       |
         |   +――――――――――| IListenerObserver             |◁― ― ―+― ― ― ― ― +
         |   |          +===============================+      |
         |   |          | + new_connection(             |      |
         |   |          |   transport_connection:       |      |
         |   |          |   TransportConnection)        |      |
         |   |          | + connection_lost()           |      |
         |   |          |   transport_connection:       |      |
         |   |          |   TransportConnection)        |      |
         |   |          +―――――――――――――――――――――――――――――――+      |
         |   |                                                 |
         |   |                                                 |
         |   |                                                 |
         |   |                                                 |
         |   |         +―――――――――――――――――――――――――――――――――――+   |
         |   |         |   Session                         |―――+
         |   |         +===================================+
         |   |         | - _transport: TransportConnection |◆―――――+
         |   |         | - _output_queue: Queue            |      |
         |   |         | + is_started: bool                |      |
         |   |         | + user: str                       |      |
         |   |         +―――――――――――――――――――――――――――――――――――+      |
         |   |         | + start()                         |      |
         |   |         | + send_message(data: Any)         |      |
         |   |         +―――――――――――――――――――――――――――――――――――+      |
         |   |                                                    |
         |   |                                                    |
         |   |         +―――――――――――――――――――――――――――――――+          |
         |   |         | TransportConnection           |――――――――――+
         |   |         +===============================+
         |   |         | + input_queue: Queue          |―――――――――――――――――+
         |   |         | + output_queue: Queue         |                 |
         |   |         +―――――――――――――――――――――――――――――――+                 |
         |   |         | + is_complete()               |                 |
         |   |         | + send_msg(data: Any)         |                 |
         |   |         | + recv_msg()                  |                 |
         |   |         +―――――――――――――――――――――――――――――――+                 |
         |   |                                                           |
         |   |                                                           |
         |   |                                                           |
         |   |      +――――――――――――――――――――――――――――――――――――――――――――――――+   |
         +―――|――――――|  TransportListener                             |   |
             |      +================================================+   |
             +―――――◆| + observer: IListenerObserver                  |   |
                    | + connections: Dict[str, TransportConnection]  |◇――+
                    +――――――――――――――――――――――――――――――――――――――――――――――――+
                    | + configure(config: map)                       |
                    | + listen()                                     |
                    | - _gen_channel_id()                            |
                    | - _channel_connected()                         |
                    | - _channel_disconnected()                      |
                    +――――――――――――――――――――――――――――――――――――――――――――――――+
                                                       △
                                                       |
                                  +――――――――――――――――――――+―――――――――――――――――――――――――+
                                  |                    |                         |
                   +―――――――――――――――――――――――――――――+     |     +―――――――――――――――――――――――――――――――――――――――――+
                   | TelegramTransportListener   |     |     | TestTcpTransportListener                |
                   +=============================+     |     +=========================================+
                   | + api_key: str              |     |     | + test_tcp_listener_input_port: int     |
                   +―――――――――――――――――――――――――――――+     |     | + test_tcp_listener_output_port: int    |
                                                       |     +―――――――――――――――――――――――――――――――――――――――――+
                                                       |
                    +――――――――――――――――――――――――――+       |
                    |  DualWSTransportListener |―――――――+
                    +==========================+
                    | + listen_domain: str     |
                    | + listen_port: int       |
                    +――――――――――――――――――――――――――+
"""