import json
from multiprocessing import Process, Queue, Manager
import platform
import pymongo
import socket
import time
from typing import Any
import unittest

from app.classes.config import get_config
from app.classes.entity_manager.app import app as app_entity_manager
from app.classes.game_handler.http_game_handler import app as app_game_handler
from app.classes.core_http.app import app as app_core_controller
from app.classes.connection_handler.main import main as connection_handler_main


if platform.system() == "Darwin": #checking OS platform and set start method for MacOS
    import multiprocessing
    multiprocessing.set_start_method('fork')


class TestCallProcess(Process):
    """
        Этот класс предназначен для запуска тестов в отдельно проессе.
        Это нужно, чтобы была возможность использовать debugger PyCharm
        и его breakpoint-ы
    """


    def __init__(self, result_store: dict, *args, **kwargs) -> None:
        super(TestCallProcess, self).__init__(*args, **kwargs)
        self.result_store = result_store

    def run(self) -> None:
        if self._target:
            result = self._target(*self._args, **self._kwargs)
        self.result_store['testsRun'] = result.testsRun

        failures_list = []
        for fail in result.failures:
            failures_list.append(fail[1])

        self.result_store['failures'] = failures_list

        error_list = []
        for fail in result.errors:
            error_list.append(fail[1])

        self.result_store['errors'] = error_list


class BaseEnvTestCase(unittest.TestCase):
    """Base environment test case"""

    _test_transport_listener_output_queue: Queue = None

    enable_entity_manager: bool = True
    enable_game_handler: bool = True
    enable_core_controller: bool = True
    enable_connection_handler: bool = True

    config = {
        'ENTITY_MANAGER': {
            'http_port': 8585,
            'http_host': '0.0.0.0',
            'debug_mode': True,
            'secret_mask': '*',
            'date_format': '%Y-%m-%d %H:%M:%S',
            'connections_aliases': {
                'default': {
                     'mongo_uri': 'mongodb://127.0.0.1:27017/test_argazy_game',
                     'mongo_db': 'test_argazy_game',
                     'mongo_host': '127.0.0.1',
                },
            },
        },
        'CONNECTION_HANDLER': {
            'service_name': 'connection_handler_1',
            'transport': 'test_tcp',
            'http_server_port': 8000,
            'test_transport_listener_tcp_host': '127.0.0.1',
            'test_transport_listener_tcp_input_port': 6000,
            'test_transport_listener_tcp_output_port': 6001,
        },
        'CORE_CONTROLLER': {
            'http_host': '0.0.0.0',
            'http_port': 8888,
        },
        'GAME_HANDLER': {
            'name': 'game_handler_1',
            'http_host': '0.0.0.0',
            'http_port': 8096,
            'debug_mode': True
        }
    }

    def __init__(self, *args, **kwargs):
        super(BaseEnvTestCase, self).__init__(*args, **kwargs)
        self._app_entity_manager = app_entity_manager
        self._app_game_handler = app_game_handler
        self._app_core_controller = app_core_controller
        self._test_transport_listener_output_queue = Queue()

    def __call__(self, *args, **kwargs):
        teamcity_obj = args[0]
        manager = Manager()

        # Prepare result dict
        result = manager.dict()
        result['testsRun'] = 0
        result['failures'] = []
        result['errors'] = []

        # Execute test in separated process
        proc = TestCallProcess(
            target=super(BaseEnvTestCase, self).__call__,
            result_store=result,
            args=args,
            kwargs=kwargs)
        proc.start()
        proc.join()

        # Put results into teamcity object
        teamcity_obj.testsRun = result['testsRun']

        for fail in result['failures']:
            teamcity_obj.failures.append((self, fail))

        for error in result['errors']:
            teamcity_obj.errors.append((self, error))

        return teamcity_obj

    def _run_core_controller(self) -> Process:
        config = get_config("CORE_CONTROLLER")
        proc: Process = Process(
            target=self._app_core_controller.run,
            kwargs={"host": "localhost", "port": config['http_port']},
        )
        proc.start()
        return proc

    def _run_game_handler(self) -> Process:
        config = get_config("GAME_HANDLER")
        proc: Process = Process(
            target=self._app_game_handler.run,
            kwargs={"host": "localhost", "port": config['http_port']},
        )
        proc.start()
        return proc

    def _run_entity_manager(self) -> Process:
        config = get_config('ENTITY_MANAGER')
        proc: Process = Process(
            target=self._app_entity_manager.run,
            kwargs={"host": "localhost", "port": config['http_port']},
        )
        proc.start()
        return proc

    def _run_connection_handler(self) -> Process:
        proc: Process = Process(target=connection_handler_main)
        proc.start()
        return proc

    def _run_connection_handler_test_output_socket(self) -> Process:
        def _run_listening():
            config = get_config("CONNECTION_HANDLER")
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.bind((
                    config['test_transport_listener_tcp_host'],
                    config['test_transport_listener_tcp_output_port']
                ))
                s.listen()
                while True:
                    conn, addr = s.accept()
                    with conn:
                        data = conn.recv(4096)
                        self._test_transport_listener_output_queue.put(data)

        proc: Process = Process(target=_run_listening)
        proc.start()
        return proc

    def read_transport_listener_message(self, attempts: int = 10, attempt_timeout: float = 0.2):
        """
        Read a message from the transport listener, that queue contains.
        In case the queue is empty it will return None.

        :return: None or json with data from queue
         """

        data: bytes = None
        for _ in range(attempts):
            try:
                data: bytes = self._test_transport_listener_output_queue.get()
            except Exception as e:
                pass
            if data:
                break
            time.sleep(attempt_timeout)

        if not data:
            return None
        else:
            return json.loads(data.decode())

    def send_transport_listener_message(self, connection_id: str, data: Any) -> bytes:
        """
        Send message and get answer from the transport listener.

        :param str connection_id: connection id that you want to send the message
        :param Any data: data of message
        :return: data received from transport listener
        """

        config = get_config('CONNECTION_HANDLER')
        b_data = json.dumps({'connection_id': connection_id, 'data': data}).encode()
        sock = socket.create_connection((
            config['test_transport_listener_tcp_host'],
            config['test_transport_listener_tcp_input_port']
        ))
        sock.send(b_data)
        data = sock.recv(1024)
        sock.close()
        try:
            return json.loads(data)
        except Exception:
            return data

    def _drop_db(self):
        if self.enable_entity_manager:
            for a in self.config['ENTITY_MANAGER']['connections_aliases'].values():
                c = pymongo.MongoClient(a["mongo_uri"])
                c.drop_database(a["mongo_db"])

    def setUp(self) -> None:
        """Before run every test entity manager, game handler, core controller, connection handler applications
         and tcp socket of test process are running"""
        config = get_config()
        config.update(self.config)

        # self._drop_db()

        if self.enable_entity_manager:
            self._app_process_entity_manager = self._run_entity_manager()
        if self.enable_game_handler:
            self._app_process_game_handler = self._run_game_handler()
        if self.enable_core_controller:
            self._app_process_core_controller = self._run_core_controller()
        if self.enable_connection_handler:
            self._app_process_connection_handler = self._run_connection_handler()
            self._connection_handler_output_socket = self._run_connection_handler_test_output_socket()
        time.sleep(0.2)

    def tearDown(self) -> None:
        """After every test terminates entity manager, game handler, core controller, connection handler applications
         and tcp socket of test process"""

        if self.enable_entity_manager:
            self._app_process_entity_manager.kill()
        if self.enable_game_handler:
            self._app_process_game_handler.kill()
        if self.enable_core_controller:
            self._app_process_core_controller.kill()
        if self.enable_connection_handler:
            self._app_process_connection_handler.kill()
            self._connection_handler_output_socket.kill()

        self._drop_db()
