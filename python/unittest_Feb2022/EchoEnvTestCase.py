import unittest
from BaseEnvTestCase import BaseEnvTestCase


class EchoEnvTestCase(BaseEnvTestCase):

    def test_echo_connection_handler(self):
        """
        Send a message with echo action to the transport listener
        and check that transport listener answer in the right format and check
        that queue of test tcp socket contains result of work transport listener.
        """
        resp = self.send_transport_listener_message(
            connection_id='15',
            data={'action': 'echo', 'request_id': '1000', 'data': 'hello'})
        self.assertEqual(resp, {'connection_created': True, 'result': 'ok'})
        resp_message = self.read_transport_listener_message()
        self.assertEqual(
            resp_message,
            {'connection_id': '15', 'data': {"type": "response", "request_id": "1000", "status": "ok", "data": "hello"}}
        )


if __name__ == '__main__':
    unittest.main()
