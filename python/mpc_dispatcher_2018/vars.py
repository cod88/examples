#!/home/anton/programming/mpc-dispatcher/bin/python3.5
import argparse
import sys
from classes.logger import getLogger

# Functions
def createArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--action', default=None)
    parser.add_argument('-pb', '--playbooks', default=None)
    parser.add_argument('-s', '--setpart', default=None)
    parser.add_argument('-w', '--where', default=None)
    parser.add_argument('-ll', '--loglevel', default="info")
    parser.add_argument('-ipl', '--itemsPerLoop', default="5")
    parser.add_argument('-verb', '--verbose', default="")
    return parser



# Get parameters
parser = createArgParser()
namespace = parser.parse_args(sys.argv[1:])
logger = getLogger(namespace.loglevel)