import sqlite3
import traceback


class LocalDB:
    def __init__(self, logger):
        self.connection = None
        self.cursor = None
        self.dbfile = './db/mpc.sqlite'
        self.log = logger

    def _connect(self):
        self.connection = sqlite3.connect(self.dbfile)
        self.cursor = self.connection.cursor()

    def _disconnect(self):
        self.cursor.close()
        self.connection.close()

    def putRow(self, row):
        sql = "insert into mpc_list ('mac', 'ip', 'address', 'city', 'ver', 'ver_num', 'reach') VALUES (" 
        for i in range(0, len(row)-1):
            sql += "?,"
        sql += "NULL,NULL,NULL)"
        self._connect()
        self.cursor.execute(sql, row[1:])
        self.connection.commit()
        self._disconnect()

    def putManyRows(self, rowsList):
        sql = "insert into mpc_list ('mac', 'ip', 'address', 'city', 'ver', 'ver_num', 'reach') VALUES (" 
        for i in range(0, len(rowsList[0])-1):
            sql += "?,"
        sql += "NULL,NULL,NULL)"
        self._connect()
        for row in rowsList:
            self.cursor.execute(sql, row[1:])
        self.connection.commit()
        self._disconnect()

    def executeQuery(self, sql):
        self._connect()
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception:
            self.log.error("Error on execute query \n{q}\n{e}".format(q = sql, e = traceback.format_exc() ))
        self._disconnect()

    def doSelect(self, sql):
        self.log.debug(sql)
        self._connect()
        try:
            self.cursor.execute(sql)
            data = self.cursor.fetchall()
        except Exception:
            self.log.warning("Error on select: {e}".format(e=traceback.format_exc()))
            data = []
        self._disconnect()
        return data

    def getMaxId(self):
        sql = "SELECT MAX(id) FROM mpc_list"
        data = self.doSelect(sql)
        return data[0][0] if data[0][0] is not None  else 0

    def getRows(self, where=''):
        sql = "SELECT * FROM mpc_list {where}".format(where = '' if where=='' else " WHERE "+where)
        return self.doSelect(sql)

    def getRowsNum(self):
        sql = "SELECT count(id) FROM mpc_list"
        data = self.doSelect(sql)
        return data[0][0] if data[0][0] is not None  else 0


# LocalDBAccessPoint = LocalDB()