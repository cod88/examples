from classes.maindb import MainDB
from classes.localdb import LocalDB
import subprocess
import string
import time
import os
from vars import namespace, logger
import requests

LDB = LocalDB(logger) # экземпляр класса для работы с локальной базой данных
MDB = MainDB(logger=logger)


def updateMpcList(params=None):
    lowlim = 0
    count= 100

    # get known the id last record
    maxId = LDB.getMaxId()
    
    # pull data from maindb
    mpcList = MDB.getMpcList(lowlim=lowlim, uplim=lowlim+count)
    lastRowsNum = 0
    while len(mpcList) > 0:
        lastRowsNum = len(mpcList)   
        logger.info("Process with segment from "+str(lowlim)+" to "+str(lowlim+count)+" records...")
        LDB.putManyRows(mpcList)
        

        # Make data in local DB properly. Search in existing rows information about micro pc and put this (information) in just added rows
        newAddedRows = LDB.getRows("id>"+str(maxId+lowlim))
        for row in newAddedRows:
            mac = row[1] if row[1] is not None else ''
            ver = str(row[5])
            OldRow = LDB.getRows("id<"+str(maxId)+" AND "+" mac='"+mac+"' AND ver <> '"+ver+"'" )
            if (len(OldRow) > 0):
                rowOld = OldRow[0]
                LDB.executeQuery("UPDATE mpc_list SET ver='{ver}', reach='{reach}', notes='{notes}' WHERE id={id}".format(reach=str(rowOld[7]), ver = str(rowOld[5]), id=str(row[0]), notes=str(rowOld[8]) ) )
        # get next data segmet from main db
        lowlim += count
        mpcList = MDB.getMpcList(lowlim=lowlim, uplim=lowlim+count)

    logger.info("Number of inserted rows is "+str(lowlim+lastRowsNum))

    # correct id field for this start from 1
    logger.debug("correct id field for this start from 1")
    LDB.executeQuery("DELETE FROM mpc_list WHERE id<="+str(maxId))
    offset = LDB.doSelect("SELECT MIN(id) FROM mpc_list")[0][0] - 1
    
    LDB.executeQuery("UPDATE mpc_list SET id=id-"+str(offset))
    maxId = LDB.getMaxId()
    LDB.executeQuery("UPDATE sqlite_sequence set seq="+str(maxId))
    LDB.executeQuery("UPDATE mpc_list SET reach=NULL WHERE reach='None'")
    LDB.executeQuery("UPDATE mpc_list SET notes=NULL WHERE notes='None'")


def listMain(params = None):
    mpcList = MDB.getMpcList()
    for row in mpcList:
        for cell in row :
            print(cell, "\t", end='')
        print("\r")


def showMenu(actions):
    print("""
Chose action whitch you want to execute:\n
        """)
    for item in actions :
        print("\t", item, " - ", actions[item][1], end="\n")
    print("""
If you run this script with parameter "-a" and set name of action, it immediately will run action which you gave.
Different actions need different parameters whitch also may be given in command line
There is this parameters:\n
\t-a [action name]
\t-pb [name of playbooks]
\t-w [where condition in SQL quert format fot insert into WHERE part]
\t-s [string which will be put in sql query as SET part]
\t-ll [loglevel for script. may be one of DEBUG, INFO, WARNING, ERROR, FATAL]
\t-ipl [number of records whitch will be processed in the iteration]
\t-verb [verbose mode for ansible playbook executing]
        """)

def showHelp(params = None):
    print("Help about this damn application")

def runPlaybook(playbook, verbose = ""):
    subprocess.call("cd ../ansible && ansible-playbook -u root playbooks/{pb} {verbose}".format(pb=playbook, verbose=verbose), shell=True)

def makeHostFileForSomeHosts(hostsList):
    hostFile = open("../ansible/hosts", 'w')
    hostFile.write("[domofonsold]\n")
    for item in hostsList:
        hostFile.write( "{name} ansible_host={ip}\n".format( name=item[0], ip=item[1]) )

    hostFile.write("""
[domofonsold:vars]
ansible_ssh_pass = password
    """)
    hostFile.close()

     

def clearSshKey(ip):
    logger.debug("Clear host from known_hosts({ip})".format(ip=ip) )
    subprocess.call("/usr/bin/ssh-keygen -f '/home/anton/.ssh/known_hosts' -R {ip_addr} 2> /dev/null".format(ip_addr=ip), shell=True)

def execPlaybooksWithFilter(params=None ) :
    error = None
    playbooks = params.playbooks
    where = params.where
    verbose = params.verbose

    if playbooks is None or playbooks == '':
        playbooks = input("Enter playbook names separated by comma ','; Playbooks should be lay in ansible/playbooks/:")
    if where is None or where == '':
        where = input("Enter where condition in string. It will be placed in SQL after 'WHERE':")
    
    if verbose is None :
        verbose = input("Enter verbose mode (v, vv, vvv, vvvv):")

    if verbose != '':
        verbose = "-"+verbose
        
    # looking around about playbooks
    ExistingPB = os.listdir('../ansible/playbooks')
    pbList = playbooks.split(",")
    
    # Check wether given playbooks exist
    for pb in pbList:
        pbExists = ExistingPB.count("{pb}.yml".format(pb=pb) )
        if pbExists < 1:
            error = "Playbook does not exist"
            logger.warning("Playbook does not exists ({pbfile}.yml)".format(pbfile=pb))
    
    if error is not None:
        return False

    devList = LDB.getRows(where) # выборка записей об устройствах
    if len(devList) == 0:
        logger.info("No records found by query")
        return False

    count = 1 # порядковый номер записи в выборке
    prevIterationCount = 1 # хранит номер первой записи в обрабатываемом сегменте
    itemsPerLoop = int(params.itemsPerLoop) # количество записей в одном сегменте. Записи берутся из выборки сегментами (групами)
    deviceGroup = [] # обрабатывамый сегмент 
    lenRows = len(devList) # общее количество записей в выборке

    for item in devList:
        address = item[3].replace(' ', '_' ).replace(',', '').replace('.', '_')
        ipAddress = item[2]
        deviceGroup.append( (address, ipAddress ) )

        if (count%itemsPerLoop == 0) or (count==lenRows) :
            logger.info("Processing items ({pc}..{c})/{total}...".format(pc=prevIterationCount, c=count, total=lenRows))
            prevIterationCount = count+1
            makeHostFileForSomeHosts(deviceGroup)
            for pb in pbList:
                for one in deviceGroup:
                    ipAddress = one[1]
                    clearSshKey(ipAddress)
                pbFileName = "{pb}.yml".format(pb=pb)
                logger.debug("Run pb:"+pbFileName)
                runPlaybook(pbFileName, verbose)   
            
            deviceGroup = []
        count+=1

def setPropertyForRecord(params=None):
    logger.debug("Got the params: {p}".format(p=params))
    setpart = params.setpart
    where = params.where

    if setpart is None or setpart == '':
        setpart = input("Enter propertyes and those values in format like if SQL query (prop='value'). It will be put in SQL after SET:")
    if where is None or where=='':
        where = input("Enter where condition in string. It will be placed in SQL after 'WHERE':")

    sql = "UPDATE mpc_list SET {s} WHERE {w}".format(s=setpart, w=where)
    logger.debug(sql)
    LDB.executeQuery(sql)

def runHttpUpgrade(params=None):
    where = params.where
    if where is None or where=='':
        where = input("Enter where condition in string. It will be placed in SQL after 'WHERE':")

    devList = LDB.getRows(where) # выборка записей об устройствах
    if len(devList) == 0:
        logger.info("No records found by query")
        return False

    for item in devList:
        ip_addr = item[2]
        print("try to get to host {host}".format(host=ip_addr))
        if ip_addr:
            url = "http://{ip}/upgrade".format(ip=ip_addr)
            try:
                r = requests.get(url, timeout=3)
            except requests.Timeout:
                print("Timeout")
                continue
        
        print("{ip} \t: {st} - {text}".format(ip=ip_addr, st=r.status_code, text=r.text))

def getVerHttp(params=None):
    where = params.where
    if where is None or where=='':
        where = input("Enter where condition in string. It will be placed in SQL after 'WHERE':")

    devList = LDB.getRows(where) # выборка записей об устройствах
    if len(devList) == 0:
        logger.info("No records found by query")
        return False

    for item in devList:
        ip_addr = item[2]
        # print("try to get to host {host}".format(host=ip_addr))
        if ip_addr:
            url = "http://{ip}/version".format(ip=ip_addr)
            try:
                r = requests.get(url, timeout=3)
            except requests.Timeout:
                print("Timeout on {ip_addr} {locate}".format(ip_addr=ip_addr, locate=item[3]) )
                continue
        
        print("{ip} \t: {st} - {text}".format(ip=ip_addr, st=r.status_code, text=r.text))

def tryOpenHttp(params=None):
    where = params.where
    if where is None or where=='':
        where = input("Enter where condition in string. It will be placed in SQL after 'WHERE':")

    devList = LDB.getRows(where) # выборка записей об устройствах
    if len(devList) == 0:
        logger.info("No records found by query")
        return False

    for item in devList:
        ip_addr = item[2]
        # print("try to get to host {host}".format(host=ip_addr))
        if ip_addr:
            url = "http://{ip}/open".format(ip=ip_addr)
            try:
                r = requests.get(url, timeout=3)
            except requests.Timeout:
                print("Timeout on {ip_addr} {locate}".format(ip_addr=ip_addr, locate=item[3]) )
                continue
        
        print("{ip} \t: {st} - {text}".format(ip=ip_addr, st=r.status_code, text=r.text))