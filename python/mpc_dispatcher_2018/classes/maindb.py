import cx_Oracle
import os 

class MainDB(object):
    
# set environment variables and init class propertyes
    def __init__(    self, 
                    logger,
                    oracle_sid='DB_IDENTIFIER',
                    oracle_home="/usr/lib/oracle/12.2/client64",
                    oracle = 'orcle',
                    tns_admin = '/usr/lib/oracle/12.2/client64/network/admin',
                    nls_lang = "AMERICAN_AMERICA.UTF8",
                    connection_string = "user_name/password@MAINDB",
                    ld_library_path = "/usr/lib/oracle/12.2/client64/lib"
                    ):
        os.environ['LD_LIBRARY_PATH']   = ld_library_path
        os.environ['ORACLE_SID']   = oracle_sid
        os.environ['ORACLE_HOME']  = oracle_home
        os.environ['ORACLE']       = oracle
        os.environ['TNS_ADMIN']    = tns_admin
        os.environ['NLS_LANG']     = nls_lang
        os.system("export LD_LIBRARY_PATH={val}".format(val=ld_library_path))
        os.system("export TNS_ADMIN={val}".format(val=tns_admin))
        os.system("export ORACLE_HOME={val}".format(val=oracle_home))

        self.connectString         = connection_string
        self.connection            = None
        self.cursor                = None
        self.data                  = tuple()
        self.logger                = logger


# connect to DB and create cursor
    def _connect(self):
        if self.connection is None :
            self.connection = cx_Oracle.connect(self.connectString)
        if self.cursor is None :
            self.cursor = self.connection.cursor();

# disconnect from db. close connection and cursor
    def _disconnect(self):
        self.cursor.close()
        self.connection.close()
        self.cursor = None
        self.connection = None

# Use existing cursor for make select from DB list of mpc
    def _selectMpcList(self, lowlim=0, uplim=20):
        
        sql = """
        SELECT * FROM
        ... here is SQL query for getting data from db ...
         """
        
        self.cursor.execute(sql)

# Entire sequence of action for get list of mpc: open connection, make select, save data in variable, close connection
    def getMpcList(self, lowlim=0, uplim=10):
        self._connect()
        self._selectMpcList(lowlim, uplim)
        self.data = self.cursor.fetchall()
        self.logger.debug("Got {c} records".format(c = len(self.data)))
        self._disconnect()
        return self.data

# mainDBAccessPoint = Maindb()