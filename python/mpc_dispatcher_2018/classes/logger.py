import logging as lg
import sys



def getLogger(lglev="info"):
	loglevel = lg.INFO

	if lglev == "debug":
		loglevel = lg.DEBUG
	if lglev == "warning":
		loglevel = lg.WARNING
	if lglev == "error":
		loglevel = lg.ERROR
	if lglev == "critical":
		loglevel = lg.CRITICAL

	logger = lg.getLogger("main")
	logger.setLevel(loglevel)

	sh = lg.StreamHandler(sys.stdout)
	fh = lg.FileHandler("worklog.log")

	formatter = lg.Formatter("%(asctime)s - %(filename)s \t %(levelname)s \t %(message)s", datefmt="%H:%M:%S")
	fh.setFormatter(formatter)
	sh.setFormatter(formatter)

	fh.setLevel(lg.WARNING)

	logger.addHandler(sh)
	logger.addHandler(fh)
	return logger
