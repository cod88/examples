#!../bin/python3.5
from classes.common import updateMpcList, listMain, showMenu, showHelp, execPlaybooksWithFilter, setPropertyForRecord, runHttpUpgrade, getVerHttp, tryOpenHttp
from vars import namespace, logger # содержит разные переменные общие для всех модулей. например параметры коммандной строки переданные при запуске
from os import path
import subprocess

print("I am not true programmer and I could not to make good programm, then copy and paste below lines in the console where You run this script before your work\n")
appDir = path.dirname( path.dirname( path.abspath(__file__) ) )
print("export LD_LIBRARY_PATH={d}/oracle_client/client64/lib".format(d=appDir))
print("export TNS_ADMIN={d}/oracle_client/client64/network/admin".format(d=appDir))
print("export ORACLE_HOME={d}/oracle_client/client64".format(d=appDir))

print("\n")

# Statement
actions = {
    "lm": (listMain, "Display list of mpc from main database" ),
    "ul": (updateMpcList, "getmpc list from MainDB and put this in localdb"),
    "help": (showHelp, "show help"),
    "q": (None, "Quit from programm" ),
    "rpb": (execPlaybooksWithFilter, "Get playbooks list, get filter conditions and execute playbooks for filtered hosts (additional params: -pb, -w, -ll, -ipl, -verb)"),
    "sv": (setPropertyForRecord, "Set values for propertyes in record given by condition (additional params: -s, -w, -ll)"),
    "runupg": (runHttpUpgrade, "Send http requesst for upgrade for devices filtered by condition (addidtional params: -w)"),
    "getVer": (getVerHttp, "Send http request for get version for devices filtered by condition (addidtional params: -w)"),
    "tryOpen": (tryOpenHttp, "Send http request for open door for devices filtered by condition (addidtional params: -w)")
    }

# Menu loop
act = namespace.action

while act != "q" :
    method = actions.get(act, (lambda arg : False, ) )[0] # this is the function corresponded to action
    method(namespace)
    if namespace.action is not None: # in case action parameter is given in prompt then just do this action and exit - not show menu
        break
    showMenu(actions)
    act = input("Your choice:")
    